Guru Point 05
=============

Instructions
------------

To get **one** extra credit point, you must do the following:

1. **Fork** this repository on [Bitbucket].

2. **Clone** your fork to a local machine.

3. In your clone, **create** a feature [branch] using [git] called
   `$NETID-GuruPoint05`.

4. In this new branch, **modify** this `README.md` to include one cool or
   interesting thing about yourself.  Follow the convention below.

5. **Commit** and **push** the new branch to your [Bitbucket] fork.

6. Submit a [pull request] to the original [Guru Point 05] repository.

You will get credit when your [pull request] has been accepted.

**Note**: The instructor will only accept a [pull request] that has no
conflicts with the existing **master** branch.  This means you need to be sure
your [pull request] can apply cleanly.  If it does not, you will need to
respond to the comments in the [pull request] to get your [branch] into proper
shape.

[Bitbucket]:    https://bitbucket.org/
[git]:          https://git-scm.com/
[branch]:       https://www.atlassian.com/git/tutorials/using-branches/
[pull request]: https://www.atlassian.com/git/tutorials/making-a-pull-request/how-it-works
[Guru Point 05]:https://bitbucket.org/CSE-20189-SP16/gurupoint_05

Peoples
-------

### Peter Bui

Some people call me Dr. Bui, but not my **mom**.  She says I'm not a real
doctor because I work on **computers** and not **people**.

### Brent Marin

I have a county named after me near San Fransisco. Google it.


### John Westhoff

Hmmm... I have a scar under my **left eyebrow** from when I hit my head on 
the edge of a staircase when I was ~two, a scar on my **left shoulder**
from when I had surgery when I was ~five, a scar on my **right elbow**
from when I fell off a scooter when I was ~twelve, and a scar on my 
**left heel** from when I tried to do a cartwheel and I kicked my foot into
a coffee table.

### Xuanyi Li

I am right-handed but I have been training myself to write with my left hand 
since six grade. When a class is boring, I take my notes with my left hand.

### Aaron Crawfis

Despite the name similarity I have actually never eaten crawfish before.... probably because I'm from Michigan and we don't have those there.

### Cameron Smick

In my free time, I go climbing at the rock wall in Rockne. Soon, once I get belay certified, I'll go on outdoor climbing trips with the Climbing Club on campus.
